"use strict";
var path  = require('path');
var fs    = require('fs');
var debug = require('debug')('xpmcore:base-class:rendering');
var renderingUtils = require('./_renderingUtil');

class RenderingComponent {

  constructor(name, ext) {
    this._name = name;
    this._util = renderingUtils;
  }

  render(req, kwargs, cb) {
    this
      .init.call(this, req, kwargs, cb)
      .then(_ => this.composeData())
      .then(_ => this._render())
      .then(templateString => this.template = templateString)
      //process
      .then(_ => {
        this.callback(null, this.template);
      })
      .catch(err => {
        debug(err);
        cb(err);
      })
  }

  composeData() {
    return Promise.resolve();
  }

  init(req, kwargs, next) {
    this.context  = kwargs;
    this.callback = next;
    this.req      = req;
    this.db       = req.db;

    return Promise.resolve();
  }

  _render() {
    var n = this.req.nunjucks || this.req.perfecto.nunjucks;
    if (!n) throw new Error("Renderer: nunjucks not found");

    var templatePath = this.getTemplatePath();

    var renderData       = {};
    renderData.req       = this.req;
    renderData.staticDir = this.req.perfecto.staticPath;
    renderData.context   = this.context;

    return new Promise((resolve, reject) => {
      fs.readFile(templatePath, 'utf-8', (err, templateString) => {
        if (err) return reject(err);
        n.renderString(templateString, renderData, (err, result) => {
          if (err) return reject(err);
          resolve(result);
        });
      });
    });
  }

  getTemplatePath(){
    return this._name; }
}

module.exports = RenderingComponent;