"use strict";
const log = require('debug')('xpmcore:base-class:model:base-model');

class BaseModel {}

module.exports = BaseModel;