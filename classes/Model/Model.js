"use strict";
const log = require('debug')('xpmcore:apps:gogo:model:model');
const BaseModel = require('./BaseModel');

/**
 * @class
 * @name {Model}
 * @property _id {ObjectId}
 * @property name {string}
 * @property order {number}
 * @property templateId {ObjectId}
 * @property parentId {ObjectId}
 * @property props {Object}
 * @property props.DisplayName {string}
 * @property _db {Db}
 */
class Model extends BaseModel {
  /**
   * @param config {Object}
   */
  constructor(config) {
    this._db = global.db;
    configure(this, config);
  }

  /**
   *
   * @returns {Db}
   */
  static getDb() {
    if (!this._db) {
      throw new Error('`_db` not set, model was not inited.');
    }
    return this._db;
  }

  /**
   *
   * @param cond
   * @returns {Promise}
   */
  static find(cond) {
    let db = this.getDb();
    if (typeof cond !== 'object') log("WARNING: `cond` is not object.");
    if (!('id' in cond)) return Promise.reject(new Error('Invalid params. cond.id required.'));
    return new Promise((resolve, reject) => {
      db
        .data.getNodeInternal(db.toObjectId(cond.id), (err, node) => {
        if (err) return reject(err);
        if (!node) return reject(new Error('Not found'));
        resolve(new this(node));
      })
    });
  }

  /**
   *
   * @param fieldName {string}
   * @param model {function}
   * @returns {Promise}
   */
  populateTreeSelector(fieldName, model) {
    let ids = this[fieldName] && this[fieldName].match(/[a-z0-9]{24}/g) || [];
    if (!ids.length) return Promise.resolve([]);
    return new Promise((resolve, reject)=> {
      this
        .constructor
        .getDb()
        .data
        .getNodes(ids, (err, nodes)=> {
          if (err) {
            log(err);
            return resolve([]);
          }
          if (!nodes.length) {
            log(new Error('Nothing found.'));
            return resolve([]);
          }
          resolve(model ? nodes.map(node => new model(node)) : nodes);
        });
    });
  }

}

/**
 *
 * @param obj {Object}
 * @param fields {Object}
 * @returns {Object}
 */
function configure(obj, fields) {
  for (let key in fields) {
    obj[key] = fields[key];
  }
  return obj;
}

/** @exports {ItemModel} */
module.exports = Model;