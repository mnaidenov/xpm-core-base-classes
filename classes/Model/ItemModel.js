"use strict";
const log = require('debug')('xpmcore:base-class:model:item-model');
const _ = require('underscore');

const Model = require('./Model');

/**
 * @class
 * @name ItemModel
 * @extends {Model}
 */
class ItemModel extends Model {

  static init(db) {
    this._db = db;
  }

  /**
   * @constructor
   * @param config {Object}
   */
  constructor(config) {
    super(config.props);
    this._attributes = _.keys(config.props);
    this._id = config._id;
    this._order = config.order;
    this._templateId = config.templateId;
    this._parentId = config.parentId;
    this._name = config.name;
  }

  get attributes() {
    return _.pick(this, this._attributes);
  }

  /**
   * @param attr
   * @private
   */
  _addAttribute(attr) {
    this._attributes.push(attr);
  }

  get attributesToSave() {
    let attributesToSave = this.attributes;
    attributesToSave.name = this._name || this.constructor.name + '-' + Date.now();
    attributesToSave.templateId = this._templateId;
    attributesToSave.parentId = this._parentId;

    if (this._order) attributesToSave.order = this._order;

    return attributesToSave;
  }

  save() {
    if (!this._id) {
      return Promise.reject(new Error('Object has no `_id` field, seems to be it is new object. User insert method instead.'));
    }
    var dataToSave = this.attributesToSave;
    log('attributesToSave', dataToSave);
    return new Promise((resolve, reject) => {
      this.constructor.getDb()
        .data.setNodeProperties(this._id, dataToSave, (err, nodes) => {
        if (err) {
          return reject(err);
        }
        caches.evict('' + this._id);
        caches.evict('' + this._parentId);
        resolve();
      });
    });
  }

  /**
   *
   * @param parentId
   * @returns {Promise}
   */
  insert(parentId) {
    parentId = parentId || this._parentId;
    if (!parentId) {
      return Promise.reject(new Error('Unable to insert without `parentId` param.'));
    }
    if (this._id) {
      return Promise.reject(new Error('Seems to be this item already exist. `_id` present in `this` object'))
    }
    var dataToSave = _.pick(this, this.attributes);
    dataToSave.name = this._name;
    dataToSave.templateId = this.constructor.getDb().toObjectId(this._templateId);
    dataToSave.parentId = this.constructor.getDb().toObjectId(parentId);
    log(dataToSave);
    return new Promise((resolve, reject) => {
      this
        .constructor
        .getDb()
        .addItem(dataToSave, (err, node) => {
          if (err) return reject(err);
          this._id = node._id;
          caches.evict('' + parentId);
          caches.evict('' + node._id);
          resolve(this);
        });
    });
  }

  toApiResponse() {
    /* @TODO: prepare attribute before send, exclude metrics and other non-used fields */
    return this.attributes;
  }

}


module.exports = ItemModel;