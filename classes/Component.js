"use strict";
const Router = require('express').Router;

class Component{
  constructor(name, isOpen){
    this.name = name;
    this._routes = [];
    this.openAPI = isOpen || false;
  }

  get routes(){
    return this._routes;
  }

  addRoute(path, handler){
    this._routes.push({path: path, handler: Router().use(handler)});
  }

}

module.exports = Component;