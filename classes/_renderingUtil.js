var renderingUtil = {};

module.exports = renderingUtil;

renderingUtil.parseExpandedImages = function parseExpandedImages(arr) {
  try {
    arr = JSON.parse(arr);
    return arr.map(imgStr => JSON.parse(imgStr).url);
  } catch (e) {
    return [];
  }
};
